package com.SistemaHotelero.SistemaHotelero.modelo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "servicio")
@Getter
@Setter


/**
 * Clase Servicio
 * atributos id, nombreServicio,precio,external_id y su respectivo mapeo
 * 
 */
public class Servicio implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Column(length = 75)
    private String nombreServicio;
    @Column(length = 30)
    private Float precio;
    @Column(length = 36)
    private String external_id;

    /**  Un servicio contiene muchos detalles */
    @OneToMany(mappedBy = "servicio", cascade = CascadeType.ALL)
    private List <Detalle> detalles;


    @CreatedDate
    @Column(name = "create_at", updatable = false, columnDefinition = "datetime default now()")
    private Date createAt;
    @LastModifiedDate
    @Column(name = "update_at", columnDefinition = "datetime default now()")
    private Date updateAt;


}
