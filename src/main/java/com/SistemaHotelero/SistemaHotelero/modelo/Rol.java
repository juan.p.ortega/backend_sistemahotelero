package com.SistemaHotelero.SistemaHotelero.modelo;

import java.io.Serializable;
import java.util.List;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "rol")
@Getter
@Setter

/**
 * Clase Rol
 * atributos id, nombreROl,autorizacion,descripcion y su respectivo mapeo
 * 
 */
public class Rol implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column(length = 50)
    private String nombreRol;
    @Column(length = 50)
    private Boolean autorizacion;
    @Column(length = 75)
    private String descripcion;
    
    /**  un rol va a tener un o muchas personas*/
    @OneToMany(mappedBy = "rol", cascade = CascadeType.ALL)
    private List <Persona> personas;

    @Column(name = "update_at", columnDefinition = "datetime default now()")
    private Date updateAt;

}
