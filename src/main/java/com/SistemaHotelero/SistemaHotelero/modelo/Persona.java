package com.SistemaHotelero.SistemaHotelero.modelo;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;


import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.SistemaHotelero.SistemaHotelero.modelo.enums.TipoIdentificacion;

import javax.persistence.CascadeType;
import javax.persistence.Column;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;


import lombok.Getter;
import lombok.Setter;
@Getter
@Setter

/**
 * Clase Persona
 * atributos id, nombre,apellidos,direccion,telefono,identificacion y su respectivo mapeo
 * 
 */
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
@Entity(name = "persona")
/** @Inheritance(strategy = InheritanceType.TABLE_PER_CLASS) esto es para la herencia a empleado y cliente, para permitir que se puedan modificar , reutilizar y extender en otras clases */
public class Persona implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(length = 50)
    private String nombre;  /** nombre, apellidos, ClienteEmpleado,direccion, telefono, identificacion,tipo, */
    
    @Column(length = 50)
    private String apellidos;

    //@Enumerated(EnumType.STRING)
    /** *private ClienteEmpleado tipoClienteEmpleado;*/

    @Column(length = 100)
    private String direccion;
    
    @Column(length = 10)
    private String telefono;
    
    @Column(length = 15, unique = true)
    private String identificacion;       /** cedula o pasaporte*/ 
    
    @Enumerated(EnumType.STRING)
    private TipoIdentificacion tipo;
    
    @Column(length = 36)
    private String external_id;
    
    /* @Column(columnDefinition = "BOOLEAN DEFAULT true")
    private Boolean Baja; */

    @OneToOne (mappedBy = "persona", cascade = CascadeType.ALL)
    private Cuenta cuenta;
    @CreatedDate
    @Column(name = "create_at", updatable = false, columnDefinition = "datetime default now()")
    private Date createAt;
    @LastModifiedDate
    @Column(name = "update_at", columnDefinition = "datetime default now()")
    private Date updateAt;

    /**  
     *MUchas personas tienen un ROL */
    @ManyToOne (cascade = CascadeType.REFRESH)
    @JoinColumn(referencedColumnName = "id", name = "id_rol")
    private Rol rol;

    /**  
     * Una persona contiene muchos reservaciones */
    @OneToMany(mappedBy = "persona", cascade = CascadeType.ALL)
    private List <Reservacion> reservaciones;



}
