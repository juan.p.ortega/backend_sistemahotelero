package com.SistemaHotelero.SistemaHotelero.modelo.enums;

/**
 * Enum para el tipo de identificacion tiene cedula ruc y pasaporte
 * @return enum
 */
public enum TipoIdentificacion {
    CEDULA ("CEDULA"),RUC("RUC"),PASAPORTE("PASAPORTE");
    private String tipo;
    private TipoIdentificacion(String tipo){
        this.tipo = tipo;
    }
    public String getTipo() {
        return tipo;
    }
}