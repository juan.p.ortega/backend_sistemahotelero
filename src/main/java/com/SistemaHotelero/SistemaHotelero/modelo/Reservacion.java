package com.SistemaHotelero.SistemaHotelero.modelo;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "reservacion")
@Getter
@Setter

/**
 * Clase Reservacion
 * atributos id, costo_total,fecha,fecha_entrada,fecha_salida,external_id,estadoFactura y su respectivo mapeo
 * 
 */
public class Reservacion implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(length = 30)
    private Double costo_total;

    @Column(length = 70)
    private LocalDate fecha;

    @Column(length = 70)
    private LocalDate fecha_entrada;
    
    @Column(length = 70)
    private LocalDate fecha_salida;

    @Column(length = 36) 
    private String external_id;

    @Column(length = 36) 
    private Boolean estadoFactura;

    @CreatedDate
    @Column(name = "create_at", updatable = false, columnDefinition = "datetime default now()")
    private Date createAt;
    @LastModifiedDate
    @Column(name = "update_at", columnDefinition = "datetime default now()")
    private Date updateAt;



    /**  con persona Reservacion es la entidad debil*/
    @ManyToOne(cascade = CascadeType.REFRESH)
    @JoinColumn(referencedColumnName = "id", unique = true, name = "id_persona")
    private Persona persona;

 
    /**  una reserva va a tener un o muchos detalles*/
    @OneToMany(mappedBy = "reservacion", cascade = CascadeType.ALL)
    private List <Detalle> detalles;
    


    
}