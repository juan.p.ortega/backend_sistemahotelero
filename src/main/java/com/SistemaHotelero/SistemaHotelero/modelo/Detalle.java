package com.SistemaHotelero.SistemaHotelero.modelo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;


import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
@Entity
@Table(name = "detalle")

/**
 * Clase Detalle
 * atributos : id,cantidad,precioUNitario,precioTotal,descripcion,external,createAt y updateAt
 */
public class Detalle implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Column(length = 100)
    private Integer cantidad;
    private Float precioUnitario;
    private Float precioTotal;
    @Column(length = 100)
    private String descripcion;
    @Column(length = 36)
    private String external_id;
    @CreatedDate
    @Column(name = "create_at", updatable = false, columnDefinition = "datetime default now()")
    private Date createAt;
    @LastModifiedDate
    @Column(name = "update_at", columnDefinition = "datetime default now()")
    private Date updateAt;


   /** MUchos detalles tienen una reservacion   detalles --> entidad debil*/ 
    @ManyToOne(cascade = CascadeType.REFRESH)
    @JoinColumn(referencedColumnName = "id", unique = true, name = "id_reservacion")
    private Reservacion reservacion;

    /** MUchos detalles tienen un servicio   detalles --> entidad debil*/ 
    @ManyToOne(cascade = CascadeType.REFRESH)
    @JoinColumn(referencedColumnName = "id", unique = true, name = "id_servicio")
    private Servicio servicio;

    /** MUchos detalles tienen una habitacion   detalles --> entidad debil*/ 
    @ManyToOne(cascade = CascadeType.REFRESH)
    @JoinColumn(referencedColumnName = "id", unique = true, name = "id_habitacion")
    private Habitacion habitacion;

}
