package com.SistemaHotelero.SistemaHotelero.modelo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "habitacion")
@Getter
@Setter

/**
 * Clase Habitacion
 * atributos id, numeroHabitacion,precioHabitacion,tipoHabitacion,descripcion,estadoHabitacion,external_id su respectivo mapeo
 * 
 */
public class Habitacion implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(length = 10,unique = true)
    private Integer numeroHabitacion;

    @Column(length = 10)
    private Double precioHabitacion;

    @Column(length = 25)
    private String tipoHabitacion;

    @Column(length = 100)
    private String descripcion;

    @Column(length = 10)
    private Boolean estadoHabitacion;

    @Column(length = 36) 
    private String external_id;

    /**  
     * Una habitacion contiene muchos detalles */
    @OneToMany(mappedBy = "habitacion", cascade = CascadeType.ALL)
    private List <Detalle> detalles;

    @CreatedDate
    @Column(name = "create_at", updatable = false, columnDefinition = "datetime default now()")
    private Date createAt;
    @LastModifiedDate
    @Column(name = "update_at", columnDefinition = "datetime default now()")
    private Date updateAt;



}
