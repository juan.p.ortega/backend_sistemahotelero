package com.SistemaHotelero.SistemaHotelero.controladores;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.SistemaHotelero.SistemaHotelero.modelo.Detalle;
import com.SistemaHotelero.SistemaHotelero.modelo.Reservacion;

/**
 * creamos DetalleRepository donde se buscar por external_id
 * @param external_id
 * @return 
 */
public interface DetalleRepository extends CrudRepository<Detalle, Integer>{
    @Query("SELECT d from Detalle d WHERE d.external_id = ?1")
    Detalle findByExternal_id(String external_id);

    @Query("SELECT d from Detalle d WHERE d.reservacion.external_id = ?1")
    List<Reservacion> buscarExternal_id(String external_id); 
    List<Reservacion> findByReservacion(Reservacion reservacion);


    //List<Detalle> finByReservacionStartsWith(String id_reservacion);
    //List<Detalle> finByPersonaStartsWith(Persona persona);


    
}
