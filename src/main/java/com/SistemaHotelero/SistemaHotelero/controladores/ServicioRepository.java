package com.SistemaHotelero.SistemaHotelero.controladores;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.SistemaHotelero.SistemaHotelero.modelo.Servicio;

/**
 * creamos ServicioRepository donde se buscar por external_id
 * @param external_id
 * @return 
 */
public interface ServicioRepository extends CrudRepository<Servicio, Integer>{
    @Query("SELECT s from Servicio s WHERE s.external_id = ?1")
    Servicio findByexternal_id(String external_id);
}
