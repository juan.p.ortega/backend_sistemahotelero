package com.SistemaHotelero.SistemaHotelero.controladores;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.SistemaHotelero.SistemaHotelero.modelo.Persona;

/**
 * creamos PersonaRepository donde se buscar por external_id e identificacion de la Persona
 * @param external_id
 * @return 
 */
public interface PersonaRepository extends CrudRepository<Persona, Integer> {
    @Query("SELECT p from persona p WHERE p.external_id = ?1")
    Persona findByExternal_id(String external_id);

    @Query("Select p from persona p where p.identificacion = ?1")
    Persona findByIdentificacion(String identificacion);

    @Query("SELECT p from persona p WHERE p.identificacion = ?1")
    Persona fyndByIdentificaionPersona(String identificacion);


}