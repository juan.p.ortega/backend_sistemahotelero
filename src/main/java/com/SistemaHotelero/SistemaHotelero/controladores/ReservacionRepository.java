package com.SistemaHotelero.SistemaHotelero.controladores;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.SistemaHotelero.SistemaHotelero.modelo.Reservacion;

/**
 * creamos ReservacionRepository donde se buscar por external_id y por fecha
 * @param external_id
 * @return 
 */
public interface ReservacionRepository extends CrudRepository<Reservacion, Integer> {
    @Query("SELECT r from Reservacion r WHERE r.external_id = ?1")
    Reservacion findByexternal_id(String external_id);

    @Query("SELECT r from Reservacion r WHERE r.fecha_salida = ?1")
    List<Reservacion>findByFechaReservacions(LocalDate fechainicio);

    /*@Query("SELECT r from Reservacion r WHERE r.fechaEmision BETWEEN ?1 and ?2")
    List<Reservacion>findByFechaEmisionBetween(LocalDate fechainicio, LocalDate fechaF);*/



}
