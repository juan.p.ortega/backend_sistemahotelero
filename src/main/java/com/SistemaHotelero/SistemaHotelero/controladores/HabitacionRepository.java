package com.SistemaHotelero.SistemaHotelero.controladores;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.SistemaHotelero.SistemaHotelero.modelo.Habitacion;

/**
 * creamos HabitacionRepository donde se buscar por external_id
 * @param external_id
 * @return 
 */
public interface HabitacionRepository extends CrudRepository<Habitacion, Integer>{
    @Query("SELECT h from Habitacion h WHERE h.external_id = ?1")
    Habitacion findByExternal_id(String external_id);
}
