package com.SistemaHotelero.SistemaHotelero.controladores;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.SistemaHotelero.SistemaHotelero.modelo.Cuenta;

/**
 * creamos CuentaRepository para manejar Cuenta, y IDs enteros
 * @param external_id
 * @return 
 */ 
public interface CuentaRepository extends CrudRepository<Cuenta, Integer>
{
    @Query("Select c from Cuenta c where c.correo = ?1")
    Cuenta findByCorreo(String correo);

    @Query("Select c from Cuenta c where c.external_id = ?1")
    Cuenta findByExternal_id(String external_id);
}
