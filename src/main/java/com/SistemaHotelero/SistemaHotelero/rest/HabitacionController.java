package com.SistemaHotelero.SistemaHotelero.rest;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.SistemaHotelero.SistemaHotelero.controladores.HabitacionRepository;
import com.SistemaHotelero.SistemaHotelero.modelo.Habitacion;
import com.SistemaHotelero.SistemaHotelero.rest.modelo_rest.HabitacionWs;
import com.SistemaHotelero.SistemaHotelero.rest.respuesta.RespuestaLista;

@RestController
@RequestMapping(value = "/api/v1")
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST, RequestMethod.HEAD })
public class HabitacionController {
    @Autowired
    private HabitacionRepository habitacionRepository;
    

    /**
     * metodo para listar las habitaciones ingresadas
     * @return
     */
    @GetMapping("/habitacion")
    public ResponseEntity listar(){
        List<Habitacion> lista = new ArrayList<>();
        List mapa = new ArrayList<>();
        habitacionRepository.findAll().forEach((p) -> lista.add(p));
        Integer cont = 0;
        for(Habitacion h : lista){
            cont++;
            HashMap aux = new HashMap<>();

            aux.put("numeroHabitacion", h.getNumeroHabitacion());
            aux.put("precioHabitacion", h.getPrecioHabitacion());
            aux.put("tipoHabitacion", h.getTipoHabitacion());
            aux.put("descripcion", h.getDescripcion());
            aux.put("estadoHabitacion", h.getEstadoHabitacion());
            aux.put("creado_en",h.getCreateAt());
            aux.put("modicado_en",h.getUpdateAt());
            aux.put("external", h.getExternal_id());
            mapa.add(aux);
        }
        return RespuestaLista.respuestaLista(mapa);
    }

    /**
     * metodo para guardar las respectivas habitaciones
     * @param habitacionWs
     * @return
     */
    @PostMapping("/habitacion/guardar")
    public ResponseEntity guardar(@Valid @RequestBody HabitacionWs habitacionWs){
        HashMap mapa = new HashMap<>();
        Habitacion habitacion = habitacionWs.cargarDatos(null);
        
        habitacionRepository.count();
        habitacion.setCreateAt(new Date());
        mapa.put("evento","Se ha registrado hbaitacion correctamente");
        habitacionRepository.save(habitacion);

        return RespuestaLista.respuesta(mapa, "OK");
    }

    /**
     * metodo para obtener una habitacion por medio de un token
     * @param external
     * @return
     */
    @GetMapping("/habitacion/obtener/{external}")
    public ResponseEntity obtener(@PathVariable String external){
        Habitacion habitacion = habitacionRepository.findByExternal_id(external);
        if (habitacion != null) {
            HashMap aux = new HashMap<>();
            aux.put("numeroHabitacion", habitacion.getNumeroHabitacion());
            aux.put("precioHabitacion", habitacion.getPrecioHabitacion());
            aux.put("tipoHabitacion", habitacion.getTipoHabitacion());
            aux.put("descripcion", habitacion.getDescripcion());
            aux.put("estadoHabitacion", habitacion.getEstadoHabitacion());
            aux.put("creado_en",habitacion.getCreateAt());
            aux.put("modicado_en",habitacion.getUpdateAt());
            aux.put("external", habitacion.getExternal_id());
            return RespuestaLista.respuestaLista(aux);
            //aux.put("",detalle.get);
        } else{
            HashMap mapa = new HashMap<>();
            mapa.put("evento", "Objeto no encontrado");
            return RespuestaLista.respuesta(mapa, "No se encontro objeto deseado");
        }
    }
}
