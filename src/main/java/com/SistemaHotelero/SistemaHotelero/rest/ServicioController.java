package com.SistemaHotelero.SistemaHotelero.rest;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.SistemaHotelero.SistemaHotelero.controladores.ServicioRepository;
import com.SistemaHotelero.SistemaHotelero.modelo.Detalle;
import com.SistemaHotelero.SistemaHotelero.modelo.Servicio;
import com.SistemaHotelero.SistemaHotelero.rest.modelo_rest.ServicioWs;
import com.SistemaHotelero.SistemaHotelero.rest.respuesta.RespuestaLista;

@RestController
@RequestMapping(value = "/api/v1")
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST, RequestMethod.HEAD })
public class ServicioController {
    @Autowired
    private ServicioRepository servicioRepository;

    /**
     * metodo para listar servicios
     * @return
     */
    @GetMapping("/servicios")
    public ResponseEntity listar() {
        List<Servicio> lista = new ArrayList<>();
        List mapa = new ArrayList<>();
        servicioRepository.findAll().forEach((p) -> lista.add(p));
        for (Servicio s : lista) {
            HashMap aux = new HashMap<>();
            aux.put("Nombre_Servicio", s.getNombreServicio());
            aux.put("precio", s.getPrecio());
            aux.put("external", s.getExternal_id());
            aux.put("Se_creo_en", s.getCreateAt());
            //aux.put("Detalle_Servicios", s.getDetalle().getDescripcion());
            mapa.add(aux);
        }
        return RespuestaLista.respuestaLista(mapa);
    }

    /**
     * metodo para guardar los servicios
     * @param servicioWs
     * @return
     */
    @PostMapping("/servicios/guardar")
    public ResponseEntity guardar(@Valid @RequestBody ServicioWs servicioWs) {
        HashMap mapa = new HashMap<>();
        Servicio servicio = servicioWs.cargaServicio(null);
        servicioRepository.count();
        servicio.setCreateAt(new Date());
        
        mapa.put("evento", "Se ha registrado el servicio correctamente");
        servicioRepository.save(servicio);
        return RespuestaLista.respuesta(mapa, "OK");

    }
}
