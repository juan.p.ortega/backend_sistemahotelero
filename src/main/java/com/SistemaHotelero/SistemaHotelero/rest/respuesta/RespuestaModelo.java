package com.SistemaHotelero.SistemaHotelero.rest.respuesta;

/**
 * Clase RespuestaModelo
 * atributos msg, code,data
 * 
 */
public class RespuestaModelo {
    private String msg;
    private String code;
    private Object data;

    public String getCode(){
        return this.code;
    }

    public void setCode(String code){
        this.code = code;
    }

    public String getMsg(){
        return this.msg;
    }

    public void setMsg(String msg){
        this.msg = msg;
    }

    public Object getData() {
        return this.data;
    }

    public void setData(Object data){
        this.data = data;
    }
}

