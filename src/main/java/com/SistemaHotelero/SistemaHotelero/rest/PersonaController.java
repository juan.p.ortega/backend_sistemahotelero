package com.SistemaHotelero.SistemaHotelero.rest;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.Optional;
import java.util.Map;
import javax.validation.Valid;

import org.springframework.dao.DataIntegrityViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.http.HttpStatus;


import com.SistemaHotelero.SistemaHotelero.controladores.PersonaRepository;
import com.SistemaHotelero.SistemaHotelero.controladores.RolRepository;
import com.SistemaHotelero.SistemaHotelero.controladores.utiles.Utilidades;
import com.SistemaHotelero.SistemaHotelero.modelo.Cuenta;
import com.SistemaHotelero.SistemaHotelero.modelo.Persona;
import com.SistemaHotelero.SistemaHotelero.modelo.Rol;

import com.SistemaHotelero.SistemaHotelero.rest.modelo_rest.PersonaWs;
import com.SistemaHotelero.SistemaHotelero.rest.respuesta.RespuestaLista;


@RestController /** Indica que esta clase es un controlador REST */ 
@RequestMapping(value = "/api/v1") /**  Especifica la raíz de la URL para todas las solicitudes que maneja este controlador */
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST, RequestMethod.HEAD })
public class PersonaController {

    @Autowired /**  Anotación que permite inyectar una dependencia de manera automática */
    private PersonaRepository personaRepository; /** Variable que almacena una instancia de PersonaRepository */
    
    @Autowired // Anotación que permite inyectar una dependencia de manera automática
    private RolRepository rolRepository; /** Variable que almacena una instancia de RolRepository */ 
    
    // Aquí se definen los métodos del controlador que manejan las solicitudes HTTP

/**
 * metodo para listar las personas
 * @return
 */
@GetMapping("/personas")
public ResponseEntity listar() 
{
    /**  Crear una lista vacía de Personas y un ArrayList para almacenar el resultado. */
    List<Persona> lista = new ArrayList<>();
    List  mapa = new ArrayList<>();

    /** Obtener todas las personas en la base de datos y agregarlas a la lista. */ 
    personaRepository.findAll().forEach((p) -> lista.add(p));

    /** Contar la cantidad de elementos en la lista de Personas. */ 
    Integer cont = 0;

    /** Para cada persona en la lista, crear un mapa con sus detalles y agregarlo al ArrayList. */
    for(Persona u: lista) 
    {
        cont++;

        // Crear un nuevo mapa para la Persona actual.
        HashMap aux = new HashMap<>();            

        // Agregar los detalles de la Persona al mapa.
        aux.put("apellidos", u.getApellidos());
        aux.put("nombres", u.getNombre());
        aux.put("direccion", u.getDireccion());
        aux.put("external", u.getExternal_id());
        aux.put("identificacion", u.getIdentificacion());
        aux.put("tipo", u.getTipo().toString());
        
        // Agregar el mapa de la Persona al ArrayList.
        mapa.add(aux);
    }

    
    return RespuestaLista.respuestaLista(mapa);
}

    //obtener persona por id
    /* @GetMapping("/personas/{id}")
    public ResponseEntity buscarPorId(@PathVariable(value = "id") Integer personaId) {
        Optional<Persona> persona = personaRepository.findById(personaId);
        if (persona.isPresent()) {
            Map<String, Object> mapa = new HashMap<>();
            Persona p = persona.get();
            mapa.put("apellidos", p.getApellidos());
            mapa.put("nombres", p.getNombre());
            mapa.put("direccion", p.getDireccion());
            mapa.put("external", p.getExternal_id());
            mapa.put("identificacion", p.getIdentificacion());
            mapa.put("tipo", p.getTipo().toString());
            return RespuestaLista.respuesta(mapa, "OK");
        } else {
            Map<String, Object> mapa = new HashMap<>();
            mapa.put("mensaje", "Persona no encontrada");
            return RespuestaLista.respuesta(mapa, "ERROR");
        }
    } */

    /**
     * metodo para buscar por identificacion
     * @param identificacion
     * @return
     */
    @GetMapping("/personas/{identificacion}")
    public ResponseEntity buscarPorIdentificacion(@PathVariable(value = "identificacion") String identificacion) {
        Persona persona = personaRepository.findByIdentificacion(identificacion);
        if (persona != null) {
            Map<String, Object> mapa = new HashMap<>();
            mapa.put("apellidos", persona.getApellidos());
            mapa.put("nombres", persona.getNombre());
            mapa.put("direccion", persona.getDireccion());
            mapa.put("external", persona.getExternal_id());
            mapa.put("identificacion", persona.getIdentificacion());
            mapa.put("tipo", persona.getTipo().toString());
            return RespuestaLista.respuesta(mapa, "OK");
        } else {
            Map<String, Object> mapa = new HashMap<>();
            mapa.put("mensaje", "Persona no encontrada");
            return RespuestaLista.respuesta(mapa, "ERROR");
        }
    }


    /**
     * metodo para registrar las personas
     * @param personaWs
     * @return
     */
    @PostMapping("/personas/guardar")
    public ResponseEntity guardar(@Valid @RequestBody PersonaWs personaWs)
    {
        HashMap mapa = new HashMap<>();
        
        Persona persona = personaWs.cargarPersona(null);
        try{

        Rol rol = rolRepository.findByTipoRol(personaWs.getNombreRol().toLowerCase());
    
        if(rol != null)
        {
            persona.setCreateAt(new Date());
            persona.setRol(rol);
            if(personaWs.getCuenta() != null)
            {
                Cuenta cuenta = personaWs.getCuenta().cargarCuenta(null);
                cuenta.setClave(Utilidades.clave(personaWs.getCuenta().getClave()));
                cuenta.setPersona(persona);
                cuenta.setCreateAt(new Date());
                cuenta.setExternal_id(UUID.randomUUID().toString());
                persona.setCuenta(cuenta);
            }
            mapa.put("evento", "Se ha registrado correctamente");
            personaRepository.save(persona);
            
            return RespuestaLista.respuesta(mapa, "OK");
        }
       
    }catch(DataIntegrityViolationException ex) {
            /** aquí manejas la excepción de violación de restricción de unicidad */
            if (ex.getMessage().contains("UK_r5vsms84ih2viwd6tatk9o5pq")) {
                /** aquí devuelves una respuesta personalizada en el cuerpo de la respuesta */
                String mensaje = "El correo electrónico " + personaWs.getCuenta().getCorreo() + " o la identificacion "+  personaWs.getIdentificacion() + " ya estan registrados";
                mapa.put("evento", mensaje);
                return RespuestaLista.respuesta(mapa, "no se pudo registrar");
            } else {
                /**
                 * aquí manejas otras excepciones
                 */
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
            }
        }
        mapa.put("evento", "no se pudo procesar la solicitud");
        return RespuestaLista.respuesta(mapa, "OK");
        
    }

    /**
     * metodo para actualizar personas
     * @param personaId
     * @param personaWs
     * @return
     */
    @PutMapping("/personas/{id}")
    public ResponseEntity actualizar(@PathVariable(value = "id") String personaId, 
                                    @Valid @RequestBody PersonaWs personaWs) {
        Persona persona = personaRepository.findByIdentificacion(personaWs.getIdentificacion());
        if (persona != null) {
            persona = personaWs.cargarPersona(persona);
            Rol rol = rolRepository.findByTipoRol(personaWs.getNombreRol().toLowerCase());
            if (rol != null) {
                persona.setRol(rol);
                if (personaWs.getCuenta() != null) {
                    Cuenta cuenta = personaWs.getCuenta().cargarCuenta(persona.getCuenta());
                    cuenta.setClave(Utilidades.clave(personaWs.getCuenta().getClave()));
                    cuenta.setPersona(persona);
                    cuenta.setCreateAt(new Date());
                    cuenta.setExternal_id(UUID.randomUUID().toString());
                    persona.setCuenta(cuenta);
                }
                personaRepository.save(persona);
                Map<String, Object> mapa = new HashMap<>();
                mapa.put("mensaje", "Persona actualizada correctamente");
                return RespuestaLista.respuesta(mapa, "OK");
            } else {
                Map<String, Object> mapa = new HashMap<>();
                mapa.put("mensaje", "Tipo de persona no válido");
                return RespuestaLista.respuesta(mapa, "ERROR");
            }
        } else {
            Map<String, Object> mapa = new HashMap<>();
            mapa.put("mensaje", "Persona no encontrada");
            return RespuestaLista.respuesta(mapa, "ERROR");
        }
    }
}
