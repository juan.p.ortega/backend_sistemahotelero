package com.SistemaHotelero.SistemaHotelero.rest;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;//
import java.util.ArrayList;
import java.util.UUID;


import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;

import com.SistemaHotelero.SistemaHotelero.controladores.CuentaRepository;
import com.SistemaHotelero.SistemaHotelero.controladores.utiles.Utilidades;
import com.SistemaHotelero.SistemaHotelero.modelo.Cuenta;
import com.SistemaHotelero.SistemaHotelero.rest.modelo_rest.CuentaWs;
import com.SistemaHotelero.SistemaHotelero.rest.respuesta.RespuestaLista;


import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;



@RestController /** es un controller especial en RESTful y equivale a la suma de @Controller y @ResponseBody */
@RequestMapping(value = "/api/v1")/**  se usa para asignar solicitudes web a los metodos de spring controller/ con esto mapeamos, aca vamos a generar nuestra URL, el end point */
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST, RequestMethod.HEAD })
public class CuentaController
{
    private static final String SECRET = "miClaveSecreta1";
    private final long EXPIRATION_TIME = 3600000; /** 1 hora en milisegundos */

    @Autowired /** nos proporciona control a la hora de querer inyectar nuestras dependencias o instancias que se almacenan en el contexto spring */
    private CuentaRepository cuentaRepository;

    /**
     * metodo para iniciar sesion con una cuenta especifica
     * @param cuentaWs
     * @return
     */
    @PostMapping("/login")
    public ResponseEntity inicioSesion(@Valid @RequestBody CuentaWs cuentaWs)
    {
        HashMap mapa = new HashMap<>();
        Cuenta cuenta = cuentaRepository.findByCorreo(cuentaWs.getCorreo());
        if(cuenta != null && Utilidades.verificar(cuentaWs.getClave(), cuenta.getClave()))
        {
            mapa.put("token",token(cuenta));
            mapa.put("external", cuenta.getExternal_id());
            mapa.put("correo", cuenta.getCorreo());
            return RespuestaLista.respuesta(mapa, "OK");
        }
        else
        {
            mapa.put("evento","Cuenta no encontrada");
            return RespuestaLista.respuesta(mapa, "Cuenta no encontrada");
        }
    }

    /**
     * Metodo para listar las cuentas creadas
     * @return
     */
    @GetMapping("/cuentas")
    public ResponseEntity listarCuentas() 
    {
        List<Cuenta> listaCuentas = new ArrayList<>();
        List  mapa = new ArrayList<>();
        cuentaRepository.findAll().forEach((p) -> listaCuentas.add(p));
        Integer cont = 0;
        for(Cuenta u: listaCuentas) 
    {
        cont++;

        /** Crear un nuevo mapa para la Persona actual.*/
        HashMap aux = new HashMap<>();            

        /**  Agregar los detalles de la Persona al mapa.*/
        aux.put("correo", u.getCorreo());
        aux.put("clave", u.getClave());
        aux.put("external", u.getExternal_id());
        
        mapa.add(aux);
    }

    return RespuestaLista.respuestaLista(mapa);
    }


    /**
     * Metodo para actualizar cuentas por medio del correo
     * @param correo
     * @param nuevaClave
     * @return
     */
    @PutMapping("/cuentas/{correo}")
    public ResponseEntity<Cuenta> actualizarCuenta(@PathVariable("correo") String correo, @RequestBody String nuevaClave) {

        Cuenta cuentaEncontrada = cuentaRepository.findByCorreo(correo);
        if (cuentaEncontrada == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        cuentaEncontrada.setClave(nuevaClave);
        cuentaEncontrada.setUpdateAt(new Date());
        Cuenta cuentaActualizada = cuentaRepository.save(cuentaEncontrada);
        return new ResponseEntity<>(cuentaActualizada, HttpStatus.OK);
    }

    /**
     * Metodo que genera el token respectivo con un determinado tiempo de expiracion
     * @param cuenta
     * @return
     */
    private String token(Cuenta cuenta)
    {
        String secretKey = SECRET;
        List<GrantedAuthority> grantedAuthorities = AuthorityUtils.commaSeparatedStringToAuthorityList(cuenta.getPersona().getRol().getNombreRol());
        String token = Jwts.builder().setId(cuenta.getExternal_id()).setSubject(cuenta.getCorreo()).setIssuedAt(new Date(System.currentTimeMillis())).setExpiration(new Date(System.currentTimeMillis()+EXPIRATION_TIME)).claim("authorities", grantedAuthorities.stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList())).signWith(SignatureAlgorithm.HS512, secretKey.getBytes()).compact();
        return "Bearer " + token;

    }

    /**
     * dar de alta
     * @param cuenta
     * @return
     */
    @PostMapping("/cuentas")
        public ResponseEntity<Cuenta> crearCuenta(@RequestBody Cuenta cuenta) {
        if (cuentaRepository.findByCorreo(cuenta.getCorreo()) != null) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
        cuenta.setExternal_id(UUID.randomUUID().toString());
        cuenta.setCreateAt(new Date());
        Cuenta cuentaCreada = cuentaRepository.save(cuenta);
        return new ResponseEntity<>(cuentaCreada, HttpStatus.CREATED);
    }

    /**
     * dar de baja
     * @param id
     * @return
     */
    @DeleteMapping("/cuentas/{id}")
    public ResponseEntity<HttpStatus> eliminarCuenta(@PathVariable("id") Integer id) {
        Cuenta cuentaEncontrada = cuentaRepository.findById(id).orElse(null);
        if (cuentaEncontrada == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        cuentaEncontrada.setUpdateAt(new Date());
        cuentaRepository.save(cuentaEncontrada);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }



}