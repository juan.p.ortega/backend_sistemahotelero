package com.SistemaHotelero.SistemaHotelero.rest.modelo_rest;

import java.util.Date;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.SistemaHotelero.SistemaHotelero.modelo.Cuenta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CuentaWs 
{
    //@empty valida que no sea ni en blanco ni nulo
    @Email(message = "Debe ingresar un correo valido")
    private String correo;
    @NotBlank(message = "La clave es requerida")
    @NotNull(message = "La clave no puede ser nula")
    private String clave;   

    /**
     * carga los atributos de cuenta
     * @param cuenta
     * @return cuenta
     */
    public Cuenta cargarCuenta(Cuenta cuenta)
    {
        if(cuenta==null) cuenta = new Cuenta();

        cuenta.setClave(clave);
        cuenta.setCorreo(correo);
        cuenta.setUpdateAt(new Date());

        return cuenta;
    }
}