package com.SistemaHotelero.SistemaHotelero.rest.modelo_rest;

import java.util.Date;
import java.util.UUID;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.SistemaHotelero.SistemaHotelero.modelo.Detalle;
import com.SistemaHotelero.SistemaHotelero.modelo.Habitacion;
import com.SistemaHotelero.SistemaHotelero.modelo.Servicio;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DetalleWs {
    //@NotBlank(message = "Campo precio unitaro es requerido")
    private Float precioUnitario;
    private Integer cantidad;
    //@NotBlank(message = "Campo precio total es requerido")
    private Float precioTotal;
    //@NotBlank(message = "Campo descripcion es requerido")
    //@Size(min = 3, max = 300, message = "El campo descripcion debe tener de 3 a 300 carecteres")
    private String descripcion;
    private String external;

    private ReservacionWS reservacionws;
    private ServicioWs serviciows;
    private Servicio servicio;
    private HabitacionWs habitacionWs;
    private Habitacion habitacion;

    /**
     * carga los atributos de detalle
     * @param detalle
     * @return detalle
     */
    public Detalle cargarDatos(Detalle detalle){
        if(detalle == null)
            detalle = new Detalle();
        detalle.setCantidad(cantidad);
        detalle.setPrecioUnitario(precioUnitario);
        detalle.setPrecioTotal(precioTotal);
        detalle.setDescripcion(descripcion);
        detalle.setExternal_id(UUID.randomUUID().toString());
        detalle.setUpdateAt(new Date());
        //if(detalle.getHabitacion() == null)
        //    habitacion = new Habitacion();
        //detalle.setHabitacion(habitacion);
        //if(detalle.getServicio() == null)
        //    servicio = new Servicio();
        //detalle.setServicio(servicio);
        //detalle.setHabitacion(habitacionWs.cargarDatos(habitacion));
        //detalle.setServicio(serviciows.cargaServicio(servicio));
        return detalle;
    }
}
