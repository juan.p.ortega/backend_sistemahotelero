package com.SistemaHotelero.SistemaHotelero.rest.modelo_rest;
import java.util.Date;
import java.util.UUID;//representa un identificador unico universal inmutable. Un UUID representa un valord de 128 bits. 

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import javax.validation.constraints.Pattern;
import com.SistemaHotelero.SistemaHotelero.modelo.Persona;
import com.SistemaHotelero.SistemaHotelero.modelo.enums.TipoIdentificacion;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PersonaWs {
    @NotBlank(message = "Campo nombre es requerido")
    @Size(min=2,max=75)
    private String nombre;
//nombre, apellidos, ClienteEmpleado,direccion, telefono, identificacion,tipo,

    @NotBlank(message = "Campo apellido es requerido")
    @Size(min=2,max=75)
    private String apellidos;

    @NotBlank(message = "Campo tipo identificacion es requerido")
    private String tipoIdentificacion;

    @NotBlank(message = "Campo direccion es requerido")
    @Size(max=75)
    private String direccion;

    @NotBlank(message = "Campo telefono es requerido")
    @Size(min=10,max=15)
    private String telefono;

    @NotBlank(message = "Campo identificacion es requerido")
    @Size(min=10,max=15)
    private String identificacion;

    @NotBlank(message = "Campo rol es requerido")
    @Pattern(regexp = "^(cliente|empleado)$", message = "El rol solo puede ser 'cliente' o 'empleado'")
    @Size(min=2,max=75)
    private String nombreRol;


    private String external_id;

    private CuentaWs cuenta;
    

    /**
     * carga los atributos de persona
     * @param persona
     * @return persona
     */
    public Persona cargarPersona(Persona persona)
    {
        if(persona == null) persona = new Persona();

        persona.setApellidos(apellidos);

        //persona.setCuenta(cuenta);
        persona.setDireccion(direccion);
        persona.setExternal_id(UUID.randomUUID().toString());
        persona.setTelefono(telefono);
        persona.setIdentificacion(identificacion);
        persona.setNombre(nombre);
        persona.setUpdateAt(new Date());
        switch(tipoIdentificacion.toLowerCase())
        {
            case "cedula":
                persona.setTipo(TipoIdentificacion.CEDULA);
                break;
            case "pasaporte":
                persona.setTipo(TipoIdentificacion.PASAPORTE);
                break;
            case "ruc":
                persona.setTipo(TipoIdentificacion.RUC);
                break;
            default:
                break;
        }

        return persona;
    }
    
}
