package com.SistemaHotelero.SistemaHotelero.rest.modelo_rest;

import java.util.Date;
import java.util.UUID;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.SistemaHotelero.SistemaHotelero.modelo.Habitacion;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HabitacionWs {
    //private Integer id;
    //@NotBlank(message = "Campo numero habitacion es requerido")
    private Integer numeroHabitacion;
    //@NotBlank(message = "Campo precio habitacion es requerido")
    private Double precioHabitacion;
    //@NotBlank(message = "Campo tipo habitacio es requerido")
    private String tipoHabitacion; // Ojo modificar tipo de habitation en Stirng
    //@NotBlank(message = "Campo descripcion es requerido")
    //@Size(min = 3, max = 300, message = "El campo descripcion debe tener de 3 a 300 carecteres")
    private String descripcion;
    //@NotBlank(message = "Campo estado es requerido boolean")
    private Boolean estadoHabitacion;
    private String external;

    /**
     * carga los atributos de habitacion
     * @param habitacion
     * @return habitacion
     */
    public Habitacion cargarDatos(Habitacion habitacion){
        if(habitacion == null)
            habitacion = new Habitacion();
        habitacion.setNumeroHabitacion(numeroHabitacion);
        habitacion.setPrecioHabitacion(precioHabitacion);
        habitacion.setTipoHabitacion(tipoHabitacion);
        habitacion.setDescripcion(descripcion);
        habitacion.setEstadoHabitacion(estadoHabitacion);
        habitacion.setExternal_id(UUID.randomUUID().toString());
        habitacion.setUpdateAt(new Date());
        return habitacion;
    }

}
