package com.SistemaHotelero.SistemaHotelero.rest.modelo_rest;

import java.util.Date;
import java.util.UUID;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.SistemaHotelero.SistemaHotelero.modelo.Servicio;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ServicioWs {
    @NotBlank(message = "Campo nombreServicio es requerido")
    @Size(min = 3, max = 200)
    private String nombreServicio;

    private Float precio;

    private String external_id;

    private DetalleWs detalleWs;

    
/**
 * carga los atributos de servicio
 * @param servicio
 * @return servicio
 */
    public Servicio cargaServicio(Servicio servicio){
        if(servicio == null){
            servicio  = new Servicio(); 
        }
        servicio.setNombreServicio(nombreServicio);
        servicio.setPrecio(precio);
        servicio.setUpdateAt(new Date());
        servicio.setExternal_id(UUID.randomUUID().toString());
        return servicio;
    }


}
