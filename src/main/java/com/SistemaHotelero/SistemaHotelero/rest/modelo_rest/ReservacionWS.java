package com.SistemaHotelero.SistemaHotelero.rest.modelo_rest;

import java.time.LocalDate;
import java.util.Date;
import java.util.UUID;

import com.SistemaHotelero.SistemaHotelero.modelo.Reservacion;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReservacionWS {

    private Double costo_total;

    private LocalDate fecha;

    private LocalDate fecha_entrada;

    private LocalDate fecha_salida;

    private Boolean estadoFactura;

    private String external_id;

    private PersonaWs personaWs;

    //private DetalleWs detalleWs;

    

    /**
     * carga los atributos de reservacion
     * @param reservacion
     * @return reservacion
     */
    public Reservacion cargaReservacion(Reservacion reservacion){
        if(reservacion == null){
            reservacion  = new Reservacion(); 
        }
        reservacion.setFecha(fecha);
        reservacion.setFecha_entrada(fecha_entrada);
        reservacion.setFecha_salida(fecha_salida);
        reservacion.setEstadoFactura(estadoFactura);
        reservacion.setCosto_total(costo_total);
        reservacion.setUpdateAt(new Date());
        reservacion.setExternal_id(UUID.randomUUID().toString());
        return reservacion;
    }
}
