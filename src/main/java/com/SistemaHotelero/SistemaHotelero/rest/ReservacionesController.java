package com.SistemaHotelero.SistemaHotelero.rest;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.SistemaHotelero.SistemaHotelero.controladores.DetalleRepository;
import com.SistemaHotelero.SistemaHotelero.controladores.PersonaRepository;
import com.SistemaHotelero.SistemaHotelero.controladores.ReservacionRepository;
import com.SistemaHotelero.SistemaHotelero.modelo.Detalle;
import com.SistemaHotelero.SistemaHotelero.modelo.Persona;
import com.SistemaHotelero.SistemaHotelero.modelo.Reservacion;
import com.SistemaHotelero.SistemaHotelero.rest.modelo_rest.PersonaWs;
import com.SistemaHotelero.SistemaHotelero.rest.modelo_rest.ReservacionWS;
import com.SistemaHotelero.SistemaHotelero.rest.respuesta.RespuestaLista;

@RestController
@RequestMapping(value = "/api/v1")
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST, RequestMethod.HEAD })
public class ReservacionesController {
    @Autowired
    private ReservacionRepository reservaRepository;
    @Autowired
    private PersonaRepository personaRepository;
    @Autowired
    private DetalleRepository detalleRepository;

    /**
     * metodo para listar reservaciones
     * @return
     */
    @GetMapping("/reservaciones")
    public ResponseEntity listar() {
        List<Reservacion> lista = new ArrayList<>();
        List mapa = new ArrayList<>();
        reservaRepository.findAll().forEach((p) -> lista.add(p));
        
        Integer cont = 0;
        String estado = "";
        for (Reservacion r : lista) {
            cont++;
            if (r.getDetalles().get(0).getHabitacion().getEstadoHabitacion() == true) {
                estado = "DISPONIBLE";
            }else{
                estado = "RESERVADO";
            }
            HashMap aux = new HashMap<>();
            aux.put("costo_Total", r.getCosto_total());
            aux.put("Estado_Factura", r.getEstadoFactura());
            aux.put("CLiente", r.getPersona().getNombre());
            aux.put("Fecha_Entrada", r.getFecha_entrada());
            aux.put("Fecha_Salida", r.getFecha_salida());
            aux.put("External_id", r.getExternal_id());
            aux.put("Creacion", r.getCreateAt());
            aux.put("Detalles_Nombre_Servicio", r.getDetalles().get(0).getServicio().getNombreServicio());
            aux.put("Detalles_N.Habitacion", r.getDetalles().get(0).getHabitacion().getNumeroHabitacion());
            aux.put("Precio_Unitario", r.getDetalles().get(0).getPrecioUnitario());
            aux.put("Precio_Total", r.getDetalles().get(0).getPrecioTotal());
            aux.put("Cantidad", r.getDetalles().get(0).getCantidad());
            aux.put("PrecioServicio", r.getDetalles().get(0).getServicio().getPrecio());
            aux.put("PrecioHabitacion", r.getDetalles().get(0).getHabitacion().getPrecioHabitacion());
            aux.put("EStadoHabitacion", estado );
            mapa.add(aux);
        }
        return RespuestaLista.respuestaLista(mapa);
    }

    /**
     * metodo para guardar reservacion con detalle y id de la persona
     * @param reservacionWS
     * @param id
     * @param externalDetalle
     * @return
     */
    @PostMapping("/reservaciones/guardar/{id}/{externalDetalle}")
    public ResponseEntity guardar(@Valid @RequestBody ReservacionWS reservacionWS,
            @PathVariable String id, @PathVariable String externalDetalle) {
        List<Reservacion> lista = new ArrayList<>();
        reservaRepository.findAll().forEach((p) -> lista.add(p));
        HashMap mapa = new HashMap<>();
        Persona persona = personaRepository.findByExternal_id(id);
        Detalle detalle = detalleRepository.findByExternal_id(externalDetalle);
        List<Detalle> listad = new ArrayList<>();
        listad.add(detalle);
        
        try {
            if (persona.getRol().getNombreRol().equals("cliente")) {
                Reservacion reservacion = reservacionWS.cargaReservacion(null);
                for (Reservacion r : lista) {
                    if ( (reservacion.getFecha_entrada().isAfter(r.getFecha_entrada())
                            || reservacion.getFecha_entrada().isEqual(r.getFecha_entrada())
                            || reservacion.getFecha_salida().isEqual(r.getFecha_salida())
                            || reservacion.getFecha_salida().isBefore(r.getFecha_salida()) ) & reservacion.getFecha_entrada().isBefore(r.getFecha_salida())) {
                        mapa.put("mensaje", "Ya hay una reservacion en esa fecha");
                        return RespuestaLista.respuesta(mapa, "ERROR"); 
                    } else {
                        reservaRepository.count();
                        reservacion.setCreateAt(new Date());
                        reservacion.setPersona(persona);
                        detalle.setReservacion(reservacion);
                        reservacion.setDetalles(listad);
                        reservaRepository.save(reservacion);
                        mapa.put("evento", "Se ha registrado la reservacion correctamente");
                        return RespuestaLista.respuesta(mapa, "OK");
                    }
                }
            } else {
                mapa.put("mensaje", "Un empleado no puede tener una reservacion");
                return RespuestaLista.respuesta(mapa, "ERROR");
            }
        } catch (DataIntegrityViolationException e) {
            if (e.getMessage().contains("UK_1gtegnripdttjo9pt4w0d5w")) {
                String mensaje = "El cliente " + persona.getNombre() + " ya esta registrado en una reservacion";
                mapa.put("evento", mensaje);
                return RespuestaLista.respuesta(mapa, "no se pudo registrar");
            }
        } catch (NullPointerException e) {
            System.out.println(e);
            mapa.put("evento", "Error  No se encuentra el cliente ");
            return RespuestaLista.respuesta(mapa, "No se pudo guardar objeto");
        }
        mapa.put("evento", "no se pudo procesar la solicitud");
        return RespuestaLista.respuesta(mapa, "OK");

    }


    /**
     * metodo para buscar la reservaciones por una fecha
     * @param fecha
     * @return
     */
    @GetMapping("/reservaciones/buscar/{fechaI}")
    public ResponseEntity buscarporFecha(@PathVariable LocalDate fecha) {
        List<Reservacion> resevaciones = new ArrayList<>();
        List mapa = new ArrayList<>();
        reservaRepository.findByFechaReservacions(fecha).forEach((r) -> resevaciones.add(r));
        for (Reservacion r : resevaciones) {
            HashMap aux = new HashMap<>();
            aux.put("costo_Total", r.getCosto_total());
            aux.put("Estado_Factura", r.getEstadoFactura());
            aux.put("CLiente", r.getPersona().getNombre());
            aux.put("Fecha_Entrada", r.getFecha_entrada());
            aux.put("Fecha_Salida", r.getFecha_salida());
            aux.put("External_id", r.getExternal_id());
            aux.put("Creacion", r.getCreateAt());
            aux.put("Detalles_Nombre_Servicio", r.getDetalles().get(0).getServicio().getNombreServicio());
            aux.put("Detalles_Nombre_Habitacion", r.getDetalles().get(0).getHabitacion().getNumeroHabitacion());
            mapa.add(aux);
        }
        return RespuestaLista.respuestaLista(mapa);

    }
}
