package com.SistemaHotelero.SistemaHotelero.rest;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.SistemaHotelero.SistemaHotelero.controladores.DetalleRepository;
import com.SistemaHotelero.SistemaHotelero.controladores.HabitacionRepository;
import com.SistemaHotelero.SistemaHotelero.controladores.ServicioRepository;
import com.SistemaHotelero.SistemaHotelero.modelo.Detalle;
import com.SistemaHotelero.SistemaHotelero.modelo.Habitacion;
import com.SistemaHotelero.SistemaHotelero.modelo.Servicio;
import com.SistemaHotelero.SistemaHotelero.rest.modelo_rest.DetalleWs;
import com.SistemaHotelero.SistemaHotelero.rest.modelo_rest.HabitacionWs;
import com.SistemaHotelero.SistemaHotelero.rest.modelo_rest.ServicioWs;
import com.SistemaHotelero.SistemaHotelero.rest.respuesta.RespuestaLista;



@RestController
@RequestMapping(value = "/api/v1")
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST, RequestMethod.HEAD })
public class DetalleController {
    @Autowired
    private DetalleRepository detalleRepository;
    @Autowired
    private HabitacionRepository habitacionRepository;
    @Autowired
    private ServicioRepository servicioRepository;


    /**
     * metodo para listar todos los detalles
     * @return ResponseEntity
     */
    @GetMapping("/detalle")
    public ResponseEntity listar(){
        List<Detalle> lista = new ArrayList<>();
        List mapa = new ArrayList<>();
        detalleRepository.findAll().forEach((p) -> lista.add(p));
        Integer cont = 0;
        for(Detalle d : lista){
            cont++;
            HashMap aux = new HashMap<>();
            aux.put("precioUnitario", d.getPrecioUnitario());
            aux.put("precioTotal", d.getPrecioTotal());
            aux.put("descripcion", d.getDescripcion());
            aux.put("external", d.getExternal_id());
            aux.put("creado_en", d.getCreateAt());
            aux.put("actualizado_en", d.getUpdateAt());
            aux.put("servicios", d.getServicio().getNombreServicio());
            aux.put("habitacion", d.getHabitacion().getTipoHabitacion());
            aux.put("N_habitacion", d.getHabitacion().getNumeroHabitacion());
            aux.put("cantidad", d.getCantidad());
            
            mapa.add(aux);
        }
        return RespuestaLista.respuestaLista(mapa);
    }

    /**
     * metodo para guardar detalles con servicios y habitaciones como parametros
     * @param detallews
     * @param externalHabitacion
     * @param externalServicio
     * @return
     */
    @PostMapping("/detalle/guardar/h/{externalHabitacion}/{externalServicio}")
    public ResponseEntity guardar(@Valid @RequestBody DetalleWs detallews,
    @PathVariable String externalHabitacion, @PathVariable String externalServicio){ //, @PathVariable String externalServicio){
        System.out.println("1**************=========================================================================*********");       
        try {
            HashMap mapa = new HashMap<>();
            List<Detalle> lista = new ArrayList<>();
            Habitacion habitacion = habitacionRepository.findByExternal_id(externalHabitacion);
            Servicio servicio = servicioRepository.findByexternal_id(externalServicio);

            habitacion.setEstadoHabitacion(false);
            detalleRepository.findAll().forEach((p) -> lista.add(p));
        
            Detalle detalle = detallews.cargarDatos(null);
            detalleRepository.count();
            detalle.setCreateAt(new Date());
            //float valor =  externalServicio;

            Float valorh = habitacion.getPrecioHabitacion().floatValue();
            Float valors = servicio.getPrecio();

            if (valorh == null || valors == null) {
                valorh = (float) 0;
                valors = (float) 0;
            }
            
            detalle.setPrecioUnitario(valorh+valors);
            detalle.setPrecioTotal(detalle.getCantidad() * (valorh+valors));
            detalle.setDescripcion(servicio.getNombreServicio() + " N.Habitacion: "+habitacion.getNumeroHabitacion());
            detalle.setHabitacion(habitacion);
            detalle.setServicio(servicio);
                
            detalleRepository.save(detalle);
            mapa.put("evento","Se ha registrado detalle correctamente");
            return RespuestaLista.respuesta(mapa, "OK");
        } catch (DataIntegrityViolationException e) {
            System.out.println(e);
            HashMap mapa = new HashMap<>();
            mapa.put("evento", "Error "+e);
            return RespuestaLista.respuesta(mapa, "No se pudo guardar objeto");
        } catch (NullPointerException e) {
            System.out.println(e);
            HashMap mapa = new HashMap<>();
            mapa.put("evento", "Error  No se encuentra un servicio o una habitacion");
            return RespuestaLista.respuesta(mapa, "No se pudo guardar objeto");
        }
        
    }

    /**
     * metodo para obtener un detalle con el external
     * @param external
     * @return
     */
    @GetMapping("/detalle/obtener/{external}")
    public ResponseEntity obtener(@PathVariable String external){
        Detalle detalle = detalleRepository.findByExternal_id(external);
        if (detalle != null) {
            HashMap aux = new HashMap<>();
            aux.put("precio_unitario",detalle.getPrecioUnitario());
            aux.put("cantidad", detalle.getCantidad());
            aux.put("precio_total",detalle.getPrecioTotal());
            aux.put("descripcion",detalle.getDescripcion());
            aux.put("external",detalle.getExternal_id());
            aux.put("creado_en",detalle.getCreateAt());
            aux.put("actualizado_en",detalle.getUpdateAt());

            return RespuestaLista.respuestaLista(aux);
            //aux.put("",detalle.get);
        } else{
            HashMap mapa = new HashMap<>();
            mapa.put("evento", "Objeto no encontrado");
            return RespuestaLista.respuesta(mapa, "No se encontro objeto deseado");
        }
    }

    /**
     * metodo para guardar
     * @param detallews
     * @return
     */
    @PostMapping("/detalle/guardar")
    public ResponseEntity guardarDetalle(@Valid @RequestBody DetalleWs detallews){
        HashMap mapa = new HashMap<>();
        Detalle detalle = detallews.cargarDatos(null);
        //detalle.getServicios();
        detalleRepository.count();
        detalle.setCreateAt(new Date());
        mapa.put("evento","Se ha registrado detalle correctamente");
        detalleRepository.save(detalle);

        return RespuestaLista.respuesta(mapa, "OK");
    }
    
    /**
     * metodo para meditar detalles
     * @param detallews
     * @return
     */
    @PostMapping("/detalle/editar")
    public ResponseEntity modificar(@Valid @RequestBody DetalleWs detallews){
        Detalle detalle = detalleRepository.findByExternal_id(detallews.getExternal());
        if (detalle != null) {
            HashMap mapa = new HashMap<>();
            detalle = detallews.cargarDatos(detalle);
            mapa.put("evento", "Se ha modificado correctamente");
            detalleRepository.save(detalle);
            return RespuestaLista.respuesta(mapa, "Ok");
        } else{
            HashMap mapa = new HashMap<>();
            mapa.put("evento","Objeto no encontrado");
            return RespuestaLista.respuesta(mapa, "No se encontro objeto deseado");
        }
    }

    
}
