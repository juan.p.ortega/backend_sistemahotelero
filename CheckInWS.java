package com.SistemaHotelero.SistemaHotelero.rest.modelo_rest;

import java.util.Date;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.SistemaHotelero.SistemaHotelero.modelo.CheckIn;
import com.SistemaHotelero.SistemaHotelero.modelo.Cuenta;
import com.SistemaHotelero.SistemaHotelero.modelo.Habitacion;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter



public class CheckInWS {
    @NotBlank(message = "Campo")
    @Size(min = 1, max = 15)
    private String precioHbitacion;
    @NotBlank(message = "Campo")
    @Size(min = 1, max = 15)
    private String cantidaPago;
    @NotBlank(message = "Campo")
    @Size(min = 1, max = 15)
    private String fechaIngreso;

    private String fechaSalida;
    private String descripcion;
    private Boolean estadoHabitacion;

    @NotBlank(message = "Campo")
    @Size(min = 1, max = 100)
    private String nombre;
    @NotBlank(message = "Campo")
    @Size(min = 1, max = 100)
    private String apellidos;
    @NotBlank(message = "Campo")
    @Size(min = 1, max = 200)
    private String direccion;
    @NotBlank(message = "Campo")
    @Size(min = 1, max = 10)
    private String telefono;
    @NotBlank(message = "Campo")
    @Size(min = 1, max = 15)
    private String identificaccion;
    @NotBlank(message = "Campo")
    @Size(min = 1, max = 15)
    private String external;

    private Habitacion habitacion;
    private Cuenta cuenta;

    public CheckIn cargarObjeto(CheckIn checkIn){
        if (checkIn == null) {
            checkIn = new CheckIn();
        }
        checkIn.setExternal_id(UUID.randomUUID().toString());
        checkIn.setPrecioHabitacion(Double.parseDouble(precioHbitacion));
        checkIn.setCantidaPago(Double.parseDouble(cantidaPago));
        //checkIn.setFechaIngreso(Date.parse(fechaIngreso));
        //checkIn.setFechaSalida(Date.valueOf(fechaSalida));
        checkIn.getHabitacion().setNumeroHabitacion(Integer.parseUnsignedInt("12"));
        checkIn.getHabitacion().setPrecioHabitacion(Double.parseDouble(precioHbitacion));
        checkIn.getHabitacion().setDescripcion(descripcion);
        checkIn.getHabitacion().setEstadoHabitacion(estadoHabitacion);
        checkIn.getCuenta().getUsuario().setNombre(nombre);
        checkIn.getCuenta().getUsuario().setApellidos(apellidos);
        checkIn.getCuenta().getUsuario().setDireccion(direccion);
        checkIn.getCuenta().getUsuario().setTelefono(telefono);
        checkIn.getCuenta().getUsuario().setIdentificacion(identificaccion);
        checkIn.getCuenta().getUsuario().setExternal_id(UUID.randomUUID().toString());
        //checkIn.setUpdateAt(new Date());

        return checkIn;
    }

}
